package pe.uni.fiorellamezar.examenfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bosphere.filelogger.FL;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    TextView textView;
    ImageView imageView;
    RelativeLayout relativeLayout;
    Button buttonSiguiente, buttonAnterior, buttonVerdadero, buttonFalso;
    ArrayList<Pregunta> preguntas = new ArrayList<>();
    int i=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FL.i(getString(R.string.onCreate));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.text_view_pregunta);
        imageView = findViewById(R.id.image_view);
        buttonAnterior = findViewById(R.id.button_anterior);
        buttonSiguiente = findViewById(R.id.button_siguiente);
        buttonVerdadero = findViewById(R.id.button_verdadero);
        buttonFalso = findViewById(R.id.button_falso);
        relativeLayout = findViewById(R.id.relative_layout);

        preguntas.add(new Pregunta(R.drawable.rojo, getString(R.string.pregunta_1), true));
        preguntas.add(new Pregunta(R.drawable.peru, getString(R.string.pregunta_2), true));
        preguntas.add(new Pregunta(R.drawable.leon, getString(R.string.pregunta_3), false));
        preguntas.add(new Pregunta(R.drawable.simpsons, getString(R.string.pregunta_4), false));
        preguntas.add(new Pregunta(R.drawable.verde, getString(R.string.pregunta_5), true));
        preguntas.add(new Pregunta(R.drawable.estadosunidos, getString(R.string.pregunta_6), false));

        textView.setText(preguntas.get(0).pregunta);
        imageView.setImageResource(preguntas.get(0).imagen);


        buttonSiguiente.setOnClickListener(v -> {
            FL.i(getString(R.string.log_button_siguiente));
            if(i == 5){
                i = 0;
            }else{
                i = i+1;
            }
            textView.setText(preguntas.get(i).pregunta);
            imageView.setImageResource(preguntas.get(i).imagen);
        });

        buttonAnterior.setOnClickListener(v -> {
            FL.i(getString(R.string.log_button_anterior));
            if(i != 0){
                i = i-1;
                textView.setText(preguntas.get(i).pregunta);
                imageView.setImageResource(preguntas.get(i).imagen);
            }
        });
        buttonVerdadero.setOnClickListener(v -> {
            FL.i(getString(R.string.log_button_verdadero));
            if(preguntas.get(i).rpta){

                Snackbar.make(relativeLayout, R.string.msg_snack_bar_correcto, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.button_snack_bar_text, v1 -> {

                        }).show();
            }else{
                Snackbar.make(relativeLayout, R.string.msg_snack_bar_incorrecto, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.button_snack_bar_text, v1 -> {

                        }).show();
            }
        });
        buttonFalso.setOnClickListener(v -> {
            FL.i(getString(R.string.log_button_falso));
            if(!preguntas.get(i).rpta){

                Snackbar.make(relativeLayout, R.string.msg_snack_bar_correcto, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.button_snack_bar_text, v1 -> {

                        }).show();
            }else{
                Snackbar.make(relativeLayout, R.string.msg_snack_bar_incorrecto, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.button_snack_bar_text, v1 -> {

                        }).show();
            }
        });
    }

    @Override
    protected void onStop() {
        FL.i(getString(R.string.onStop));
        super.onStop();


    }
}